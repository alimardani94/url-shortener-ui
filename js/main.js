function openNav() {
    $("#mySidenav").css('width', "210px");
    $("#main").css('marginLeft', "210px")
        .css('opacity', "0.94");
    $('#openNavBtn').hide();
}

function closeNav() {
    $("#mySidenav").css('width', "0");
    $("#main").css('marginLeft', "0")
        .css('opacity', "1");
    $('#openNavBtn').show();
}

function copyToClipboard(element) {
    var $temp = $("<input>");
    $("body").append($temp);
    $temp.val($(element).text()).select();
    document.execCommand("copy");
    $temp.remove();
}

$(document).ready(function () {
    $('#search').on('keyup', function (e) {
        var value = $(this).val();
        if (e.which === 13) {
            $.ajax({
                type: 'POST',
                url: url,
                data: {url: value},
                dataType: 'json',
                beforeSend: function () {
                    $('#loading').show();
                    $('.errorContainer').hide();
                    $('#errorMessage').html('');
                    $('.linkContainer').hide();
                    $('#link').html('');
                },
                success: function (data) {
                    $('.linkContainer').slideDown('fast');
                    $('#link').html(data.shortedUrl);
                    console.log(data);
                },
                error: function (xhr) {
                    console.log(xhr,'hgfhgf');

                    var message =  xhr.responseJSON.message;
                    $('.errorContainer').fadeIn('fast').delay(6000).fadeOut('fast');
                    $('#errorMessage').html(message);
                },
                complete: function () {
                    $('#loading').hide();
                },

            });
        }
    })
});
